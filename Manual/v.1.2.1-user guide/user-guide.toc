\contentsline {chapter}{\numberline {1}What is \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{}}{3}
\contentsline {chapter}{\numberline {2}Installing \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{}}{5}
\contentsline {chapter}{\numberline {3}Getting Started}{8}
\contentsline {section}{\numberline {3.1}Prolog Programmer Quick Start}{8}
\contentsline {section}{\numberline {3.2}Developer Quick Start}{9}
\contentsline {chapter}{\numberline {4}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Basics}{11}
\contentsline {section}{\numberline {4.1}Structure of a \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Engine}{11}
\contentsline {section}{\numberline {4.2}Prolog syntax}{12}
\contentsline {section}{\numberline {4.3}Configuration of a \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Engine}{15}
\contentsline {section}{\numberline {4.4}Built-in predicates}{16}
\contentsline {subsection}{\numberline {4.4.1}Control management}{16}
\contentsline {subsection}{\numberline {4.4.2}Term Unification and Management}{17}
\contentsline {subsection}{\numberline {4.4.3}Knowledge-base management}{17}
\contentsline {subsection}{\numberline {4.4.4}Operators and Flags Management}{18}
\contentsline {subsection}{\numberline {4.4.5}Libraries Management}{19}
\contentsline {subsection}{\numberline {4.4.6}Directives}{19}
\contentsline {chapter}{\numberline {5}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Libraries}{21}
\contentsline {section}{\numberline {5.1}BasicLibrary}{23}
\contentsline {subsection}{\numberline {5.1.1}Predicates}{23}
\contentsline {subsubsection}{Type Testing}{23}
\contentsline {subsubsection}{Term Creation, Decomposition and Unification}{24}
\contentsline {subsubsection}{Occurs Check}{25}
\contentsline {subsubsection}{Expression and Term Comparison}{25}
\contentsline {subsubsection}{Finding Solutions}{26}
\contentsline {subsubsection}{Control Management}{26}
\contentsline {subsubsection}{Clause Retrival, Creation and Destruction}{27}
\contentsline {subsubsection}{Operator Management}{28}
\contentsline {subsubsection}{Flag Management}{28}
\contentsline {subsubsection}{Actions on Theories and Engines}{28}
\contentsline {subsubsection}{Spy Events}{29}
\contentsline {subsubsection}{Auxiliary predicates}{30}
\contentsline {subsection}{\numberline {5.1.2}Functors}{31}
\contentsline {subsection}{\numberline {5.1.3}Operators}{31}
\contentsline {section}{\numberline {5.2}ISOLibrary}{33}
\contentsline {subsection}{\numberline {5.2.1}Predicates}{33}
\contentsline {subsubsection}{Type Testing}{33}
\contentsline {subsubsection}{Atoms Processing}{33}
\contentsline {subsection}{\numberline {5.2.2}Functors}{34}
\contentsline {subsection}{\numberline {5.2.3}Operators}{35}
\contentsline {subsection}{\numberline {5.2.4}Flags}{35}
\contentsline {section}{\numberline {5.3}DCGLibrary}{36}
\contentsline {subsection}{\numberline {5.3.1}Predicates}{37}
\contentsline {subsection}{\numberline {5.3.2}Operators}{37}
\contentsline {section}{\numberline {5.4}IOLibrary}{37}
\contentsline {subsection}{\numberline {5.4.1}Predicates}{38}
\contentsline {subsubsection}{General I/O}{38}
\contentsline {subsubsection}{I/O and Theories Helpers}{40}
\contentsline {subsubsection}{Random Generation of Numbers}{40}
\contentsline {chapter}{\numberline {6}Accessing Java from \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{}}{42}
\contentsline {section}{\numberline {6.1}Mapping data structures}{42}
\contentsline {section}{\numberline {6.2}General predicates description}{42}
\contentsline {section}{\numberline {6.3}Predicates}{48}
\contentsline {subsection}{\numberline {6.3.1}Method Invocation, Object and Class Creation}{48}
\contentsline {subsection}{\numberline {6.3.2}Java Array Management}{50}
\contentsline {subsection}{\numberline {6.3.3}Helper Predicates}{51}
\contentsline {section}{\numberline {6.4}Functors}{51}
\contentsline {section}{\numberline {6.5}Operators}{51}
\contentsline {section}{\numberline {6.6}Java Library Examples}{52}
\contentsline {subsection}{\numberline {6.6.1}RMI Connection to a Remote Object}{52}
\contentsline {subsection}{\numberline {6.6.2}Java Swing GUI from \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}}{52}
\contentsline {subsection}{\numberline {6.6.3}Database access via JDBC from \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}}{53}
\contentsline {subsection}{\numberline {6.6.4}Dynamic compilation}{54}
\contentsline {chapter}{\numberline {A}Related Documents and Publications}{57}
\contentsline {chapter}{\numberline {B}Document History}{58}
