%=======================================================================
\chapter{How to Develop New Libraries}
\label{ch:howto-develop-libraries}
%=======================================================================

Libraries are \tuprolog{}'s way to achieve the desired characteristics
of minimality, dynamic configurability, and straightforward
Prolog-to-Java integration.
%
Libraries are reflection-based, and can be written both in Prolog
and Java: other languages may be used indirectly, via JNI (Java
Native Interface).
%
At the \tuprolog{} side, exploiting a library written in Java
requires no pre-declaration of the new built-ins, nor any other
special mechanism: all is needed is the presence of the
corresponding \texttt{.class} library file in the proper location
in the file system.

\section{Implementation details}

Syntactically, a library developed in Java must extend the base
abstract class \texttt{alice.tuprolog.Library}, provided within
the \tuprolog{} package, and define new \textit{predicates} and/or
\textit{evaluable functors} and/or \textit{directives} in the form
of methods, following a simple signature convention.
%
In particular, new predicates must adhere to the signature:
%
\begin{center}
\small\tt
    public boolean <\textit{pred name}>\_<\textit{N}>(\textit{T1} arg1,
\textit{T2} arg2, ...,\textit{Tn} argN)
\end{center}
%
while evaluable functors must follow the form:
%
\begin{center}
    \small\tt
    public Term <\textit{eval funct name}>\_<\textit{N}>(\textit{T1} arg1,
\textit{T2} arg2, ...,\textit{Tn} argN)
\end{center}
%
and directives must be provided with the signature:
%
\begin{center}
    \small\tt
    public void <\textit{dir name}>\_<\textit{N}>(\textit{T1} arg1,
\textit{T2} arg2, ..., \textit{Tn} argN)
\end{center}
%
where \textit{T1}, \textit{T2}, ... \textit{Tn} are \texttt{Term} or derived
classes, such as \texttt{Struct}, \texttt{Var}, \texttt{Long}, etc., defined in
the \tuprolog{} package, constituting  the Java counterparts of
the corresponding Prolog data types.
%
The parameters represent the arguments actually passed to the built-in
predicate, functor, or directive.

%
A library defines also a new piece of theory, which is collected
by the Prolog engine through a call to the library method \texttt{String
getTheory()}.
%
By default, this method returns an empty theory: libraries which need to
add a Prolog theory must override it accordingly.
%
Note that only the external representation of a library's theory is
constrained to be in \texttt{String} form; the internal implementation
can be freely chosen by the library designer. However, using a Java
\texttt{String} for wrapping a library's Prolog code guarantees
self-containment while loading libraries through remote mechanisms such
as RMI.

\begin{table}[h]
    %
    \caption{Predicate and functor definitions in Java and their use
    in a \tuprolog{} program.\labeltab{java-preds}}
    %
    \begin{center}{\small\tt
    \begin{tabular}{p{10cm}|p{3.25cm}}
     \hline
     & \\
    \textit{// sample library} & \textit{\% tuProlog test program}\\
     import alice.tuprolog.*; & \\
     & \\
    public class TestLibrary extends Library \{                            &
 test :-\\
    ~~\textit{// builtin functor sum(A,B)}                          & ~~N is sum(5,6),\\
    ~~public Term sum\_2(Number arg0, Number arg1)\{    & ~~println(N).\\
    ~~~~float   arg0 = arg0.floatValue();        & \\
    ~~~~float   arg1 = arg1.floatValue();        & \\
    ~~~~return new Float(arg0+arg1);                                & \\
    ~~\}                                                            & \\
    ~~\textit{// builtin predicate println(Message)}                & \\
    ~~public boolean println\_1(Term arg)\{                      & \\
    ~~~~System.out.println(arg);                                   & \\
    ~~~~return true;                                                & \\
    ~~\}                                                            & \\
    \}                                                              & \\
     & \\
     \hline
    \end{tabular}
    }\end{center}
\end{table}

\xt{java-preds} shows a couple of examples about how a predicate
(such as \texttt{println/1}) and an evaluable functor (such as
\texttt{sum/2}) can be defined in Java and exploited from
\tuprolog{}.
%
The Java method \texttt{sum\_2}, which implements the evaluable
functor \texttt{sum/2}, is passed two \texttt{Number} terms (5 and 6)
which are then used (via \texttt{getTerm}) to retrieve the two
(float) arguments to be summed.
%
In the same way, method \texttt{println\_1}, which implements the
predicate \texttt{println/1}, receives \texttt{N} as \texttt{arg},
and retrieves its actual value via \texttt{getTerm}: since this is
a predicate, a boolean value (\texttt{true} in this case) is returned.
%

The developer of a library may face two corner case as far as method
naming is concerned: the first happens when the name of the
predicate, functor or directive she is defining contains a symbol
which cannot legally appear in a Java method's name; the second
occurs when he has to define a predicate and a directive with the
same Prolog signature, which Java would not be able to tell apart
because it cannot distinguish signatures of methods differing for
their return type only.
%
To overcome this kind of issues, a {\em synonym map} can be
constructed under the form of an array of \texttt{String} arrays,
and returned by the appropriate \texttt{getSynonymMap} method,
defined as abstract by the \texttt{Library} class. In both the cases
described above, another name must be chosen for the Prolog
executable element the library's developer want to define: then, by
means of the synonym map, that fake name can be associated with the
real name and the type of the element, be it a predicate, a functor
or a directive.
%
For example, if a definition for an evaluable functor representing
addition is needed, but the symbol \texttt{+} cannot appear in a
Java method's name, a method called \texttt{add} can be defined and
associated to its original Prolog name and its function by inserting
the array \texttt{\{"+", "add", "functor"\}} in the synonym map.

\section{Library Name}

% Lib name
%
By default, the name of the library coincides with the full class name of the
class implementing it.
%
However, it is possible to define explicitly the name of a library by
overriding the \texttt{getName} method, and returning as a string
the real name.
%
For example:
%
\begin{verbatim}
package acme;
import alice.tuprolog.*;
public class MyLib_ver00 extents Library {
    public String getName(){
        return "MyLibrary";
    }
    ...
}
\end{verbatim}

This class defines a library called \texttt{MyLibrary}.
%
It can be loaded into a Prolog engine by using the
\texttt{loadLibrary} method on the Java side, or a
\texttt{load\_library} built-in predicate on the Prolog side,
specifying the full class name (\texttt{acme.MyLib\_{ve00}}).
%
It can be unloaded then dynamically using the \texttt{unloadLibrary}
method (or the corresponding \texttt{unload\_library} built-in),
specifying instead the \textit{library name} (\texttt{MyLibrary}).
