\contentsline {chapter}{\numberline {1}What is \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{}}{6}{chapter.1}
\contentsline {chapter}{\numberline {2}Installing \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{}}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Installation in Java}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Installation in .NET}{11}{section.2.2}
\contentsline {section}{\numberline {2.3}Installation in Android}{11}{section.2.3}
\contentsline {section}{\numberline {2.4}Installation in Eclipse}{12}{section.2.4}
\contentsline {chapter}{\numberline {3}Getting Started}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} for the Prolog User}{16}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Editing theories}{18}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Solving goals}{20}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Debugging support}{22}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Dynamic library management}{23}{subsection.3.1.4}
\contentsline {section}{\numberline {3.2}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} for the Java Developer}{25}{section.3.2}
\contentsline {section}{\numberline {3.3}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} for the .NET Developer}{32}{section.3.3}
\contentsline {section}{\numberline {3.4}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} for the Android User}{32}{section.3.4}
\contentsline {chapter}{\numberline {4}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Basics}{38}{chapter.4}
\contentsline {section}{\numberline {4.1}Predicate categories}{38}{section.4.1}
\contentsline {section}{\numberline {4.2}Syntax}{39}{section.4.2}
\contentsline {section}{\numberline {4.3}Engine configurability}{41}{section.4.3}
\contentsline {section}{\numberline {4.4}Exception support}{42}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Error classification}{43}{subsection.4.4.1}
\contentsline {section}{\numberline {4.5}Built-in predicates}{44}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Control management}{44}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Term unification and management}{46}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Knowledge base management}{48}{subsection.4.5.3}
\contentsline {subsection}{\numberline {4.5.4}Operator and flag management}{50}{subsection.4.5.4}
\contentsline {subsection}{\numberline {4.5.5}Library management}{52}{subsection.4.5.5}
\contentsline {subsection}{\numberline {4.5.6}Directives}{53}{subsection.4.5.6}
\contentsline {chapter}{\numberline {5}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Libraries}{56}{chapter.5}
\contentsline {section}{\numberline {5.1}BasicLibrary}{58}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Predicates}{58}{subsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.1.1}Type Testing}{58}{subsubsection.5.1.1.1}
\contentsline {subsubsection}{\numberline {5.1.1.2}Term Creation, Decomposition and Unification}{59}{subsubsection.5.1.1.2}
\contentsline {subsubsection}{\numberline {5.1.1.3}Occurs Check}{61}{subsubsection.5.1.1.3}
\contentsline {subsubsection}{\numberline {5.1.1.4}Expression and Term Comparison}{62}{subsubsection.5.1.1.4}
\contentsline {subsubsection}{\numberline {5.1.1.5}Finding Solutions}{62}{subsubsection.5.1.1.5}
\contentsline {subsubsection}{\numberline {5.1.1.6}Control Management}{63}{subsubsection.5.1.1.6}
\contentsline {subsubsection}{\numberline {5.1.1.7}Clause Retrieval, Creation and Destruction}{64}{subsubsection.5.1.1.7}
\contentsline {subsubsection}{\numberline {5.1.1.8}Operator Management}{65}{subsubsection.5.1.1.8}
\contentsline {subsubsection}{\numberline {5.1.1.9}Flag Management}{65}{subsubsection.5.1.1.9}
\contentsline {subsubsection}{\numberline {5.1.1.10}Actions on Theories and Engines}{66}{subsubsection.5.1.1.10}
\contentsline {subsubsection}{\numberline {5.1.1.11}Spy Events}{68}{subsubsection.5.1.1.11}
\contentsline {subsubsection}{\numberline {5.1.1.12}Auxiliary predicates}{68}{subsubsection.5.1.1.12}
\contentsline {subsection}{\numberline {5.1.2}Functors}{70}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Operators}{70}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}ISOLibrary}{73}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Predicates}{73}{subsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.1.1}Type Testing}{73}{subsubsection.5.2.1.1}
\contentsline {subsubsection}{\numberline {5.2.1.2}Atoms Processing}{73}{subsubsection.5.2.1.2}
\contentsline {subsection}{\numberline {5.2.2}Functors}{75}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Operators}{76}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Flags}{76}{subsection.5.2.4}
\contentsline {section}{\numberline {5.3}IOLibrary}{77}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Predicates}{77}{subsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.1.1}General I/O}{77}{subsubsection.5.3.1.1}
\contentsline {subsubsection}{\numberline {5.3.1.2}Helper Predicates}{82}{subsubsection.5.3.1.2}
\contentsline {subsubsection}{\numberline {5.3.1.3}Random Generation of Numbers}{84}{subsubsection.5.3.1.3}
\contentsline {section}{\numberline {5.4}ThreadLibrary}{84}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Predicates}{85}{subsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.1.1}Creating and deleting threads}{85}{subsubsection.5.4.1.1}
\contentsline {subsubsection}{\numberline {5.4.1.2}Inter-thread communication via queues}{88}{subsubsection.5.4.1.2}
\contentsline {subsubsection}{\numberline {5.4.1.3}Thread synchronization via mutual exclusion}{90}{subsubsection.5.4.1.3}
\contentsline {subsection}{\numberline {5.4.2}Examples}{92}{subsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.2.1}Factorial of two numbers}{93}{subsubsection.5.4.2.1}
\contentsline {subsubsection}{\numberline {5.4.2.2}Father and child communicating via a public queue}{93}{subsubsection.5.4.2.2}
\contentsline {subsubsection}{\numberline {5.4.2.3}Father and children communicating via a private queue}{94}{subsubsection.5.4.2.3}
\contentsline {subsubsection}{\numberline {5.4.2.4}Synchronizing thread interactions}{96}{subsubsection.5.4.2.4}
\contentsline {subsubsection}{\numberline {5.4.2.5}Flattening and manipulating lists}{98}{subsubsection.5.4.2.5}
\contentsline {section}{\numberline {5.5}DCGLibrary}{98}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Predicates}{101}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Operators}{102}{subsection.5.5.2}
\contentsline {section}{\numberline {5.6}ISOIOLibrary}{103}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}Predicates}{105}{subsection.5.6.1}
\contentsline {subsection}{\numberline {5.6.2}Options}{114}{subsection.5.6.2}
\contentsline {section}{\numberline {5.7}SocketLibrary}{118}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}Predicates}{119}{subsection.5.7.1}
\contentsline {subsection}{\numberline {5.7.2}Operators}{123}{subsection.5.7.2}
\contentsline {subsection}{\numberline {5.7.3}Use from the Java side: term hierarchy extension}{123}{subsection.5.7.3}
\contentsline {chapter}{\numberline {6}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} Exceptions}{125}{chapter.6}
\contentsline {section}{\numberline {6.1}Exceptions in ISO Prolog}{125}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Error classification}{127}{subsection.6.1.1}
\contentsline {section}{\numberline {6.2}Exceptions in \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}}{128}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Examples}{128}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Handling Java/.NET Exceptions from \unhbox \voidb@x \hbox {{\sf {tu}}Prolog}}{130}{subsection.6.2.2}
\contentsline {chapter}{\numberline {7}Multi-paradigm programming in Prolog and Java}{131}{chapter.7}
\contentsline {section}{\numberline {7.1}Using Java from Prolog: \textit {JavaLibrary}}{131}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Type mapping}{132}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Creating and accessing objects: an overview}{133}{subsection.7.1.2}
\contentsline {subsubsection}{\numberline {7.1.2.1}Examples}{137}{subsubsection.7.1.2.1}
\contentsline {subsubsection}{\numberline {7.1.2.2}Registering object bindings}{139}{subsubsection.7.1.2.2}
\contentsline {subsection}{\numberline {7.1.3}Predicates}{140}{subsection.7.1.3}
\contentsline {subsubsection}{\numberline {7.1.3.1}Object creation, class compilation and method invocation}{140}{subsubsection.7.1.3.1}
\contentsline {subsubsection}{\numberline {7.1.3.2}Array management}{143}{subsubsection.7.1.3.2}
\contentsline {subsubsection}{\numberline {7.1.3.3}Helper predicates}{145}{subsubsection.7.1.3.3}
\contentsline {subsection}{\numberline {7.1.4}Functors}{145}{subsection.7.1.4}
\contentsline {subsection}{\numberline {7.1.5}Operators}{145}{subsection.7.1.5}
\contentsline {subsection}{\numberline {7.1.6}Examples}{146}{subsection.7.1.6}
\contentsline {subsubsection}{\numberline {7.1.6.1}RMI Connection to a Remote Object}{146}{subsubsection.7.1.6.1}
\contentsline {subsubsection}{\numberline {7.1.6.2}A Swing GUI}{146}{subsubsection.7.1.6.2}
\contentsline {subsubsection}{\numberline {7.1.6.3}Database access via JDBC}{146}{subsubsection.7.1.6.3}
\contentsline {subsubsection}{\numberline {7.1.6.4}Dynamic compilation}{149}{subsubsection.7.1.6.4}
\contentsline {subsection}{\numberline {7.1.7}Handling Java Exceptions}{150}{subsection.7.1.7}
\contentsline {subsubsection}{\numberline {7.1.7.1}Java exception examples}{152}{subsubsection.7.1.7.1}
\contentsline {section}{\numberline {7.2}Using Prolog from Java: \textit {the Java API}}{156}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}A Taxonomy of Prolog types in Java}{156}{subsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.1.1}Further notes about \texttt {Term}s}{157}{subsubsection.7.2.1.1}
\contentsline {subsection}{\numberline {7.2.2}Prolog engines, theories and libraries}{159}{subsection.7.2.2}
\contentsline {subsubsection}{\numberline {7.2.2.1}Further notes about \texttt {Prolog} engines}{159}{subsubsection.7.2.2.1}
\contentsline {subsection}{\numberline {7.2.3}Examples}{161}{subsection.7.2.3}
\contentsline {subsubsection}{\numberline {7.2.3.1}Appending lists}{161}{subsubsection.7.2.3.1}
\contentsline {subsubsection}{\numberline {7.2.3.2}Exploiting a theory from clause list}{164}{subsubsection.7.2.3.2}
\contentsline {subsubsection}{\numberline {7.2.3.3}A console-based Prolog interpreter}{164}{subsubsection.7.2.3.3}
\contentsline {subsection}{\numberline {7.2.4}Support to relative paths in consulting Prolog sub-files}{166}{subsection.7.2.4}
\contentsline {subsection}{\numberline {7.2.5}Registering object bindings}{167}{subsection.7.2.5}
\contentsline {subsection}{\numberline {7.2.6}Capturing the Prolog output in Java}{169}{subsection.7.2.6}
\contentsline {section}{\numberline {7.3}Augmenting Prolog via Java:\\developing new libraries}{169}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}Syntactic conventions}{171}{subsection.7.3.1}
\contentsline {subsubsection}{\numberline {7.3.1.1}Capturing exceptions raised in libraries}{173}{subsubsection.7.3.1.1}
\contentsline {subsubsection}{\numberline {7.3.1.2}Capturing the Java output in Prolog}{173}{subsubsection.7.3.1.2}
\contentsline {subsubsection}{\numberline {7.3.1.3}Naming issues}{173}{subsubsection.7.3.1.3}
\contentsline {subsection}{\numberline {7.3.2}Hybrid Java+Prolog libraries}{177}{subsection.7.3.2}
\contentsline {subsection}{\numberline {7.3.3}Library loading issues}{179}{subsection.7.3.3}
\contentsline {subsection}{\numberline {7.3.4}Library Name}{180}{subsection.7.3.4}
\contentsline {section}{\numberline {7.4}Augmenting Java via Prolog:\\the P@J framework}{180}{section.7.4}
\contentsline {subsection}{\numberline {7.4.1}Term taxonomy}{182}{subsection.7.4.1}
\contentsline {subsection}{\numberline {7.4.2}Examples}{183}{subsection.7.4.2}
\contentsline {chapter}{\numberline {8}Multi-paradigm programming in Prolog and .NET}{189}{chapter.8}
\contentsline {section}{\numberline {8.1}A bit of history}{189}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} 2.1 and CSharpLibrary}{189}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} 2.1.3: CSharpLibrary + exceptions}{190}{subsection.8.1.2}
\contentsline {subsection}{\numberline {8.1.3}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}{} 2.2 and CLILibrary}{190}{subsection.8.1.3}
\contentsline {section}{\numberline {8.2}IKVM Basics}{192}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Dynamic vs. Static modality}{192}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}Class loading issues}{193}{subsection.8.2.2}
\contentsline {subsection}{\numberline {8.2.3}The other way: writing .NET applications in Java}{194}{subsection.8.2.3}
\contentsline {section}{\numberline {8.3}\unhbox \voidb@x \hbox {{\sf {tu}}Prolog}.NET now}{196}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Highlights}{196}{subsection.8.3.1}
\contentsline {section}{\numberline {8.4}Using .NET from Prolog: OOLibrary}{198}{section.8.4}
\contentsline {subsubsection}{\numberline {8.4.0.1}Motivation}{198}{subsubsection.8.4.0.1}
\contentsline {subsubsection}{\numberline {8.4.0.2}Language Conventions}{199}{subsubsection.8.4.0.2}
\contentsline {subsubsection}{\numberline {8.4.0.3}OOLibrary: predicates}{200}{subsubsection.8.4.0.3}
\contentsline {subsection}{\numberline {8.4.1}Examples}{201}{subsection.8.4.1}
\contentsline {subsection}{\numberline {8.4.2}Handling .NET Exceptions}{205}{subsection.8.4.2}
\contentsline {section}{\numberline {8.5}Using Prolog from .NET: the API}{206}{section.8.5}
\contentsline {section}{\numberline {8.6}Augmenting Prolog via .NET:\\developing new libraries}{206}{section.8.6}
\contentsline {subsection}{\numberline {8.6.1}Capturing exceptions raised in .NET libraries}{210}{subsection.8.6.1}
\contentsline {subsection}{\numberline {8.6.2}Capturing the .NET output in Prolog}{210}{subsection.8.6.2}
\contentsline {section}{\numberline {8.7}Augmenting .NET via Prolog:\\the P@J framework revised}{212}{section.8.7}
\contentsline {subsection}{\numberline {8.7.1}P@.NET via code generators}{213}{subsection.8.7.1}
\contentsline {section}{\numberline {8.8}Putting everything together}{214}{section.8.8}
\contentsline {subsection}{\numberline {8.8.1}Example: Multi-language TicTacToe}{217}{subsection.8.8.1}
\contentsline {chapter}{\numberline {9}Version history}{221}{chapter.9}
\contentsline {subsection}{\numberline {9.0.2}Version 2.0}{221}{subsection.9.0.2}
\contentsline {subsection}{\numberline {9.0.3}From Version 2.0 to Version 2.0.1}{222}{subsection.9.0.3}
\contentsline {subsection}{\numberline {9.0.4}From Version 2.0.1 to Version 2.1}{224}{subsection.9.0.4}
\contentsline {subsection}{\numberline {9.0.5}From Version 2.1 to Version 2.2}{227}{subsection.9.0.5}
\contentsline {subsection}{\numberline {9.0.6}From Version 2.2 to Version 2.3.0}{227}{subsection.9.0.6}
\contentsline {subsection}{\numberline {9.0.7}From Version 2.3.0 to Version 2.3.1}{229}{subsection.9.0.7}
\contentsline {subsection}{\numberline {9.0.8}From Version 2.3.1 to Version 2.4}{229}{subsection.9.0.8}
\contentsline {subsection}{\numberline {9.0.9}From Version 2.4 to Version 2.5}{229}{subsection.9.0.9}
\contentsline {subsection}{\numberline {9.0.10}From Version 2.5 to Version 2.6}{229}{subsection.9.0.10}
\contentsline {subsection}{\numberline {9.0.11}From Version 2.6 to Version 2.7}{230}{subsection.9.0.11}
\contentsline {section}{\numberline {9.1}Acknowledgments}{230}{section.9.1}
